var getPayload = context.getVariable("request.content");
var payload = JSON.parse(getPayload);

var requestbillingAccountNotes = {};
var billingAccounts = [];
var billingAccount = {};
var addresses = [];


var billingAccountElement = payload.billingAccounts[0];

if(billingAccountElement !== undefined) {
    
    if(billingAccountElement.firstName !== undefined) {
        billingAccount.firstName = billingAccountElement.firstName.toUpperCase();
    } if(billingAccountElement.lastName !== undefined) {
        billingAccount.lastName = billingAccountElement.lastName.toUpperCase();
    }  if(billingAccountElement.email !== undefined) {
        billingAccount.email = billingAccountElement.email.toUpperCase();
    } if(billingAccountElement.phones !== undefined) {
        billingAccount.phones = billingAccountElement.phones;
    } if(billingAccountElement.addresses !== undefined && billingAccountElement.addresses.length > 0) {
        
        var size = billingAccountElement.addresses.length;
        
        for(i=0; i<size; i++) {
            var address = {};
            if(billingAccountElement.addresses[i].line1 !== undefined) {
                address.line1=billingAccountElement.addresses[i].line1.toUpperCase();
            } if(billingAccountElement.addresses[i].line2 !== undefined) {
                address.line2=billingAccountElement.addresses[i].line2.toUpperCase();
            } if(billingAccountElement.addresses[i].line3 !== undefined) {
                address.line3=billingAccountElement.addresses[i].line3.toUpperCase();
            } if(billingAccountElement.addresses[i].line4 !== undefined) {
                address.line4=billingAccountElement.addresses[i].line4.toUpperCase();
            } if(billingAccountElement.addresses[i].state !== undefined) {
                address.state=billingAccountElement.addresses[i].state.toUpperCase();
            } if(billingAccountElement.addresses[i].city !== undefined) {
                address.city=billingAccountElement.addresses[i].city.toUpperCase();
            } if(billingAccountElement.addresses[i].postalCode !== undefined) {
                address.postalCode=billingAccountElement.addresses[i].postalCode;
            } if(billingAccountElement.addresses[i].country !== undefined) {
                address.country=billingAccountElement.addresses[i].country;
            } if(billingAccountElement.addresses[i].type !== undefined) {
                address.type=billingAccountElement.addresses[i].type.toUpperCase();
            }
         addresses.push(address);   
        }
        
        billingAccount.addresses = addresses;
        
    } if(billingAccountElement.type !== undefined) {
        billingAccount.type = billingAccountElement.type.toUpperCase();
    } if(billingAccountElement.debtorBranch !== undefined) {
        billingAccount.debtorBranch = billingAccountElement.debtorBranch.toUpperCase();
    }  if(billingAccountElement.cdgId !== undefined) {
        billingAccount.cdgId = billingAccountElement.cdgId.toUpperCase();
    }
    
    billingAccounts.push(billingAccount);
    if(payload.correlationId !== undefined) {
        requestbillingAccountNotes.correlationId = payload.correlationId
    }
    
    requestbillingAccountNotes.billingAccounts = billingAccounts;
}


context.setVariable("request.content", JSON.stringify(requestbillingAccountNotes));
 