var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var responseCode = responseBody.applicationError.code;
    var accountElement = responseBody.accounts.AccountElement;
    var responseObject = {};
    var billingAccounts = [];
    var finalObject = {};
    
    if(responseCode == 10){
        context.setVariable('response.status.code', 201);
        
        // map firstName
        if (accountElement.firstName != "NULL") {
            finalObject.firstName = accountElement.firstName;
        } else {
            finalObject.firstName = "";
        }
        
        // map lastName
        if (accountElement.companyName != "NULL" && accountElement.companyName !== null) {
            finalObject.lastName = accountElement.companyName;
        } else {
            finalObject.lastName = "";
        }
        
        // map type 
        finalObject.type = "Non Energy";
        
        // map email
        if (accountElement.email != "NULL") {
            finalObject.email = accountElement.email;
        } else {
            finalObject.email = "";
        }
        
        // map companyName
        finalObject.companyName = "";
        
        // map companyNameDescription
        if (accountElement.operatingCompanyDescription != "NULL" && accountElement.operatingCompanyDescription !== null) {
            finalObject.companyNameDescription = accountElement.operatingCompanyDescription;
        } else {
            finalObject.companyNameDescription = "";
        }
        
        // map debtorBranch
        if (accountElement.debtorBranchDescription != "NULL") {
            finalObject.debtorBranch = accountElement.debtorBranchDescription;
        } else {
            finalObject.debtorBranch = "";
        }
        
        // map billingId
        finalObject.billingId = accountElement.number.toString();
        
        
        // map checkDigitBillingId
        if (accountElement.fullAccountNumber != "NULL") {
            finalObject.checkDigitBillingId = accountElement.fullAccountNumber.toString();
        } else {
            finalObject.checkDigitBillingId = "";
        }
        
        // map phones
        var phoneTypes = {
            4: "MOBILE",
            1: "HOME",
            2: "WORK"
        };
        var phonesObjectArray = [];
        var cellPhoneObject = accountElement.cellPhone;
        var homePhoneObject = accountElement.homePhone;
        var workPhoneObject = accountElement.workPhone;
        
        if (typeof cellPhoneObject !== null) {
            if (cellPhoneObject.areaCode !== "NULL") {
                var cell = {};
                cell.type = phoneTypes[4];
                cell.countryCode = "1";
                cell.number = cellPhoneObject.areaCode + '' + cellPhoneObject.prefix + '' + cellPhoneObject.suffix;
                phonesObjectArray.push(cell);
            }
        }
        
        if (typeof homePhoneObject !== null) {
            if (homePhoneObject.areaCode !== "NULL") {
                var home = {};
                home.type = phoneTypes[1];
                home.countryCode = "1";
                home.number = homePhoneObject.areaCode + '' + homePhoneObject.prefix + '' + homePhoneObject.suffix;
                phonesObjectArray.push(home);
            }
        }
        
        if (typeof workPhoneObject !== null) {
            if (workPhoneObject.areaCode !== "NULL") {
                var work = {};
                work.type = phoneTypes[2];
                work.countryCode = "1";
                work.number = workPhoneObject.areaCode + '' + workPhoneObject.prefix + '' + workPhoneObject.suffix;
                phonesObjectArray.push(work);
            }
        }
        
        if(phonesObjectArray.length > 0) {
            finalObject.phones = phonesObjectArray;
        }
        
        
        
        billingAccounts.push(finalObject);
        responseObject.billingAccounts = billingAccounts;
        
    } else {
        var responseMessage = responseBody.applicationError.logMessage;
        var responsetype = responseBody.applicationError.type;
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", responseMessage);
        context.setVariable("responsetype", responsetype);
    }
    
    
    context.setVariable("finalResponse", JSON.stringify(responseObject));
    
    
}
