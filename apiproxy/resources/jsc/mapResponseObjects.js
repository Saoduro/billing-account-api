var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var correlationId = responseBody.requestID;
    var successResponse = responseBody.account;
    if (typeof successResponse != 'undefined'){
        var addresses = successResponse.addresses;
        var cellPhoneObject = successResponse.cellPhone;
        var homePhoneObject = successResponse.homePhone;
        var workPhoneObject = successResponse.workPhone;
        
    }
    
    
    var finalObject = {};
    var billingAccounts = [];

    var phonesObjectArray = [];
    
    var stateCodes = {
        "AL": "Alabama",
        "AK": "Alaska",
        "AS": "American Samoa",
        "AZ": "Arizona",
        "AR": "Arkansas",
        "CA": "California",
        "CO": "Colorado",
        "CT": "Connecticut",
        "DE": "Delaware",
        "DC": "District Of Columbia",
        "FM": "Federated States Of Micronesia",
        "FL": "Florida",
        "GA": "Georgia",
        "GU": "Guam",
        "HI": "Hawaii",
        "ID": "Idaho",
        "IL": "Illinois",
        "IN": "Indiana",
        "IA": "Iowa",
        "KS": "Kansas",
        "KY": "Kentucky",
        "LA": "Louisiana",
        "ME": "Maine",
        "MH": "Marshall Islands",
        "MD": "Maryland",
        "MA": "Massachusetts",
        "MI": "Michigan",
        "MN": "Minnesota",
        "MS": "Mississippi",
        "MO": "Missouri",
        "MT": "Montana",
        "NE": "Nebraska",
        "NV": "Nevada",
        "NH": "New Hampshire",
        "NJ": "New Jersey",
        "NM": "New Mexico",
        "NY": "New York",
        "NC": "North Carolina",
        "ND": "North Dakota",
        "MP": "Northern Mariana Islands",
        "OH": "Ohio",
        "OK": "Oklahoma",
        "OR": "Oregon",
        "PW": "Palau",
        "PA": "Pennsylvania",
        "PR": "Puerto Rico",
        "RI": "Rhode Island",
        "SC": "South Carolina",
        "SD": "South Dakota",
        "TN": "Tennessee",
        "TX": "Texas",
        "UT": "Utah",
        "VT": "Vermont",
        "VI": "Virgin Islands",
        "VA": "Virginia",
        "WA": "Washington",
        "WV": "West Virginia",
        "WI": "Wisconsin",
        "WY": "Wyoming"
    };
    
    var addressTypes = {
        A: "MAILING",
        B: "MAILING",
        J: "TAX"
    };
    
    var phoneTypes = {
        CellPhone: "MOBILE",
        homePhone: "HOME",
        workPhone: "WORK"
    };
    
    //map correlationId
    if (typeof responseBody.requestID != 'undefined' & responseBody.requestID != "NULL") {
        finalObject.correlationId = responseBody.requestID;
    } else {
        finalObject.correlationId = "";
    }
    
    //mapping billingAccounts
    var billingAccountsObject = {};
    
    //map first name
    if (typeof successResponse.firstName != 'undefined' && successResponse.firstName != "NULL") {
        billingAccountsObject.firstName = successResponse.firstName;
    } else {
        billingAccountsObject.firstName = "";
    }
    
    //map lastName
    if (typeof successResponse.lastName != 'undefined' && successResponse.lastName != "NULL") {
        billingAccountsObject.lastName = successResponse.lastName;
    } else if (typeof successResponse.companyName != 'undefined' && successResponse.companyName != "NULL"){
        billingAccountsObject.lastName = successResponse.companyName;
    } else {
        billingAccountsObject.lastName = "";
    }
    
    //mad type
    if (typeof successResponse.type != 'undefined' && successResponse.type != "NULL") {
        billingAccountsObject.type = successResponse.type;
    } else {
        billingAccountsObject.type = "";
    }
    
    //mad email
    if (typeof successResponse.email != 'undefined' && successResponse.email != "NULL") {
        billingAccountsObject.email = successResponse.email;
    } else {
        billingAccountsObject.email = "";
    }
    
    //mad debtorBranchDescription
    if (typeof successResponse.debtorBranchDescription != 'undefined' && successResponse.debtorBranchDescription != "NULL") {
        billingAccountsObject.debtorBranchDescription = successResponse.debtorBranchDescription;
    } else {
        billingAccountsObject.debtorBranchDescription = "";
    }
    
    //mad operatingCompanyDescription
    if (typeof successResponse.operatingCompanyDescription != 'undefined' && successResponse.operatingCompanyDescription != "NULL") {
        billingAccountsObject.operatingCompanyDescription = successResponse.operatingCompanyDescription;
    } else {
        billingAccountsObject.operatingCompanyDescription = "";
    }
    
    //mad checkDigitBillingId
    if (typeof successResponse.fullAccountNumber != 'undefined' && successResponse.fullAccountNumber != "NULL") {
        billingAccountsObject.checkDigitBillingId = successResponse.fullAccountNumber;
    } else {
        billingAccountsObject.checkDigitBillingId = "";
    }
    
    //mad billingId
    if (typeof successResponse.number != 'undefined' && successResponse.number != "NULL") {
        billingAccountsObject.billingId = successResponse.number;
    } else {
        billingAccountsObject.billingId = "";
    }
    
    // address mapping
    if (typeof addresses != 'undefined' && addresses !== "NULL") {
        var addressObjectArray = [];
        var addressElements = addresses.AddressElement;
        
        if (typeof addressElements != 'undefined' && Array.isArray(addressElements)) {
            addressElements.forEach(function (addressElement) {
                var expirationDate = addressElement.expirationDate;
            var isAddressExpired = false;
            if (expirationDate !== "") {
                var currentDate = Date.now();
                var expDate = Date.parse(expirationDate);
                if (currentDate > expDate) {
                    isAddressExpired = true;
                }
            }
            
            if (isAddressExpired === false) {
            
                var addressObject = {};
                //map address type
                if (typeof addressElement.type != 'undefined' && addressElement.type !== "NULL") {
                    var type = addressElement.type;
                    addressObject.type = addressTypes[type];
                } else {
                    addressObject.type = "";
                }
                
                //map address line1 
                if (typeof addressElement.line1 != 'undefined' && addressElement.line1 !== "NULL") {
                    addressObject.line1 = addressElement.line1;
                } else {
                    addressObject.line1 = "";
                }
                
                //map address line2 
                if (typeof addressElement.line2 != 'undefined' && addressElement.line2 !== "NULL") {
                    addressObject.line2 = addressElement.line2;
                } else {
                    addressObject.line2 = "";
                }
                
                //map address line3 
                addressObject.line3 = "";
                //map address line4 
                addressObject.line4 = "";
                
                //map address city 
                if (typeof addressElement.line3 != 'undefined' && addressElement.line3 !== "NULL" && addressElement.line3.slice(0, -3) !== null) {
                    addressObject.city = addressElement.line3.slice(0, -3);
                } else {
                    addressObject.city = "";
                }
                
                //map address state
                if (typeof addressElement.line3 != 'undefined' && addressElement.line3 !== "NULL" && addressElement.line3.slice(-2) !== null) {
                    var state = addressElement.line3.slice(-2);
                    addressObject.state = stateCodes[state];
                } else {
                    addressObject.state = "";
                }
                
                    
                //map postal code
                if (typeof addressElement.zip4 != 'undefined' && addressElement.zip4 !== "NULL") {
                    addressObject.postalCode = addressElement.zip+ '-' + addressElement.zip4;
                } else if (typeof addressElement.zip != 'undefined' && addressElement.zip !== "NULL"){
                    addressObject.postalCode = addressElement.zip;
                } else {
                    addressObject.postalCode = "";
                }
                
                //map address country
                addressObject.country = "United States";
                
                //map address effectiveDate
                if (typeof addressElement.effectiveDate != 'undefined' && addressElement.effectiveDate !== "NULL") {
                    addressObject.effectiveDate = addressElement.effectiveDate;
                } else {
                    addressObject.effectiveDate = "";
                }
                
                //map address id
                if (typeof addressElement.number != 'undefined' && addressElement.number !== "NULL") {
                    addressObject.id = addressElement.number;
                } else {
                    addressObject.id = "";
                }
                
                addressObjectArray.push(addressObject);
            }
            });
        } else if (typeof addressElements != 'undefined') {
            var expirationDate = addressElements.expirationDate;
            var isAddressExpired = false;
            if (typeof expirationDate != 'undefined' && expirationDate !== "") {
                var currentDate = Date.now();
                var expDate = Date.parse(expirationDate);
                if (currentDate > expDate) {
                    isAddressExpired = true;
                }
            }
            
            if (isAddressExpired === false) {
            
                var addressObject = {};
                //map address type
                if (typeof addressElements.type != 'undefined' && addressElements.type !== "NULL") {
                    var type = addressElements.type;
                    addressObject.type = addressTypes[type];
                } else {
                    addressObject.type = "";
                }
                
                //map address line1 
                if (typeof addressElements.line1 != 'undefined' && addressElements.line1 !== "NULL") {
                    addressObject.line1 = addressElements.line1;
                } else {
                    addressObject.line1 = "";
                }
                
                //map address line2 
                if (typeof addressElements.line2 != 'undefined' && addressElements.line2 !== "NULL") {
                    addressObject.line2 = addressElements.line2;
                } else {
                    addressObject.line2 = "";
                }
                
                //map address line3 
                addressObject.line3 = "";
                //map address line4 
                addressObject.line4 = "";
                
                //map address city 
                if (typeof addressElements.line3 != 'undefined' && addressElements.line3 !== "NULL" && addressElements.line3.slice(0, -3) !== null) {
                    addressObject.city = addressElements.line3.slice(0, -3);
                } else {
                    addressObject.city = "";
                }
                
                //map address state
                if (typeof addressElements.line3 != 'undefined' && addressElements.line3 !== "NULL" && addressElements.line3.slice(-2) !== null) {
                    var state = addressElements.line3.slice(-2);
                    addressObject.state = stateCodes[state];
                } else {
                    addressObject.state = "";
                }
                
                    
                //map postal code
                if (typeof addressElements.zip4 != 'undefined' && addressElements.zip4 != "NULL") {
                    addressObject.postalCode = addressElements.zip+ '-' + addressElements.zip4;
                } else {
                    addressObject.postalCode = "";
                }
                
                //map address country
                addressObject.country = "United States";
                
                //map address effectiveDate
                if (typeof addressElements.effectiveDate != 'undefined' && addressElements.effectiveDate !== "NULL") {
                    addressObject.effectiveDate = addressElements.effectiveDate;
                } else {
                    addressObject.effectiveDate = "";
                }
                
                //map address id
                if (typeof addressElements.number != 'undefined' && addressElements.number !== "NULL") {
                    addressObject.id = addressElements.number;
                } else {
                    addressObject.id = "";
                }
                
                addressObjectArray.push(addressObject);
            }
        }
        if(addressObjectArray.length > 0) {
            billingAccountsObject.addresses = addressObjectArray;
        }
    }
    
    //phones mapping
    if (typeof cellPhoneObject != "undefined") {
        if (cellPhoneObject.areaCode !== "NULL") {
            var cell = {};
            cell.type = phoneTypes.CellPhone;
            cell.countryCode = "1";
            cell.number = cellPhoneObject.areaCode + '' + cellPhoneObject.prefix + '' + cellPhoneObject.suffix;
            phonesObjectArray.push(cell);
        }
    }
    
    if (typeof homePhoneObject !== 'undefined') {
        if (homePhoneObject.areaCode !== "NULL") {
            var home = {};
            home.type = phoneTypes.homePhone;
            home.countryCode = "1";
            home.number = homePhoneObject.areaCode + '' + homePhoneObject.prefix + '' + homePhoneObject.suffix;
            phonesObjectArray.push(home);
        }
    }
    
    if (typeof workPhoneObject !== 'undefined') {
        if (workPhoneObject.areaCode !== "NULL") {
            var work = {};
            work.type = phoneTypes.workPhone;
            work.countryCode = "1";
            work.number = workPhoneObject.areaCode + '' + workPhoneObject.prefix + '' + workPhoneObject.suffix;
            phonesObjectArray.push(work);
        }
    }
    
    if(phonesObjectArray.length > 0) {
        billingAccountsObject.phones = phonesObjectArray;
    }
    
    
    //mad actualCurrentBalance
    if (typeof successResponse.actualCurrentBalance != 'undefined' && successResponse.actualCurrentBalance != "NULL" && successResponse.actualCurrentBalance.value !== null && successResponse.actualCurrentBalance.value != "NULL") {
        billingAccountsObject.actualCurrentBalance = successResponse.actualCurrentBalance.value;
    } else {
        billingAccountsObject.actualCurrentBalance = "";
    }
    
    //mad currentBalance
    if (typeof successResponse.currentBalance != 'undefined' && successResponse.currentBalance != "NULL" && successResponse.currentBalance.value !== null && successResponse.currentBalance.value != "NULL") {
        billingAccountsObject.currentBalance = successResponse.currentBalance.value;
    } else {
        billingAccountsObject.currentBalance = "";
    }
    
    //mad pastDueBalance
    if (typeof successResponse.pastDueBalance != 'undefined' && successResponse.pastDueBalance != "NULL" && successResponse.pastDueBalance.value !== null && successResponse.pastDueBalance.value != "NULL") {
        billingAccountsObject.pastDueBalance = successResponse.pastDueBalance.value;
    } else {
        billingAccountsObject.pastDueBalance = "";
    }
    
    //map isOnEnergyAssistance
    if (successResponse.eapEnrolled === 0) {
        billingAccountsObject.isOnEnergyAssistance = "false";
    } else {
        billingAccountsObject.isOnEnergyAssistance = "true";
    }
    
    //map lastPayment
       
   var lastPaymentArray = [];
   var lastPayment = {};
   if (successResponse.payments !== "NULL") {
       
   

    if (successResponse.payments.PaymentElement.datePosted !== "NULL") {
        
        lastPayment.datePosted = successResponse.payments.PaymentElement.datePosted;
    } else {
        lastPayment.datePosted = "";
    }
    if (successResponse.payments.PaymentElement.dateReceived != "NULL") {
        lastPayment.dateReceived = successResponse.payments.PaymentElement.dateReceived;
    } else {
        lastPayment.dateReceived = "";
    }
    if (successResponse.payments.PaymentElement.amount.value != "NULL") {
       lastPayment.amount = successResponse.payments.PaymentElement.amount.value;
    } else {
        lastPayment.amount = "";
    }
    if (successResponse.payments.PaymentElement.methodId != "NULL") {
        lastPayment.method = successResponse.payments.PaymentElement.methodId;
    } else {
        lastPayment.method = "";
    }
    if (successResponse.payments.PaymentElement.methodDescription != "NULL") {
        lastPayment.methodDescription = successResponse.payments.PaymentElement.methodDescription;
    } else {
        lastPayment.methodDescription = "";
    }
    if (successResponse.payments.PaymentElement.status != "NULL") {
        lastPayment.status = successResponse.payments.PaymentElement.status;
    } else {
        lastPayment.status = "";
    }
    if (successResponse.payments.PaymentElement.statusDescription != "NULL") {
        lastPayment.statusDescription = successResponse.payments.PaymentElement.statusDescription;
    } else {
        lastPayment.statusDescription = "";
    }
    if (successResponse.payments.PaymentElement.receiptId != "NULL") {
        lastPayment.receiptId = successResponse.payments.PaymentElement.receiptId;
    } else {
       lastPayment.receiptId = "";
    }
    
   } else {
       lastPayment.datePosted = "";
       lastPayment.dateReceived = "";
       lastPayment.amount = "";
       lastPayment.method = "";
       lastPayment.methodDescription = "";
       lastPayment.status = "";
       lastPayment.statusDescription = "";
       lastPayment.receiptId = "";
   }
	
       lastPaymentArray.push(lastPayment);
        billingAccountsObject.lastPayment = lastPaymentArray;
    
    
    //map laststatement
    var lastStatementArray = [];
    var lastStatement = {};
    if (successResponse.statements !== "NULL") {

    if ( successResponse.statements.StatementElement.number != "NULL") {
        lastStatement.number = successResponse.statements.StatementElement.number;
    } else {
        lastStatement.number = "";
    }
    if (successResponse.statements.StatementElement.date != "NULL") {
        lastStatement.date = successResponse.statements.StatementElement.date;
    } else {
        lastStatement.date = "";
    }
    if (successResponse.statements.StatementElement.dueDate != "NULL") {
        lastStatement.dueDate = successResponse.statements.StatementElement.dueDate;
    } else {
        lastStatement.dueDate = "";
    }
    if (successResponse.statements.StatementElement.dueDate != "NULL") {
        lastStatement.dueDate = successResponse.statements.StatementElement.dueDate;
    } else {
        lastStatement.dueDate = "";
    }
    if (successResponse.statements.StatementElement.openingBalance != "NULL") {
        
        if (successResponse.statements.StatementElement.openingBalance.value === "NULL") {
            lastStatement.openingBalance = "0.00";
        } else {
            lastStatement.openingBalance = successResponse.statements.StatementElement.openingBalance.value;
        }    
    } else {
        lastStatement.openingBalance = "";
    }  
    
    if (successResponse.statements.StatementElement.totalDue.value != "NULL") {
        lastStatement.closingBalance = successResponse.statements.StatementElement.totalDue.value;
    } else {
        lastStatement.closingBalance = "";
    }
     
    } else {
       lastStatement.number = "";
       lastStatement.date = "";
       lastStatement.dueDate = "";
       lastStatement.openingBalance = "";
       lastStatement.closingBalance = "";
   }
     lastStatementArray.push(lastStatement);
     billingAccountsObject.lastStatement = lastStatementArray;
        
    billingAccounts.push(billingAccountsObject);    
    finalObject.billingAccounts = billingAccounts;    
    context.setVariable("finalResponse", JSON.stringify(finalObject));
}

