var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var responseCode = responseBody.applicationError.code;
    var responseMessage = responseBody.applicationError.logMessage;
    var initialRequest = context.getVariable("initialRequest");
    var request = JSON.parse(initialRequest);
    var correlationId = responseBody.requestID;
    var finalObject = {};
    
    //Response Mapping
    if (typeof responseCode != 'undefined' && responseCode >= 100) {
        var error = {};
        var billingId = context.getVariable("apigee.billingId");
        // mapping correlationId
        finalObject.correlationId = correlationId;
        request.billingId = billingId;
        // mapping billingAccount
        finalObject.billingAccount = request;
        error.code = "Error";
        error.description = responseMessage;
        // mapping error
        finalObject.error = error;
        
        context.setVariable("isValidResponse", false);
    } else if (typeof responseCode != 'undefined' && responseCode == 10) {
        context.setVariable("isValidResponse", true);
        var accountElement = responseBody.accounts.AccountElement;
        var addresses = responseBody.accounts.AccountElement.addresses.AddressElement;
        var billingId = responseBody.accounts.AccountElement.number;
        
        var addressTypes = {
            A: "MAILING",
            J: "TAX"
        };
        var stateCodes = {
            "AL": "Alabama",
            "AK": "Alaska",
            "AS": "American Samoa",
            "AZ": "Arizona",
            "AR": "Arkansas",
            "CA": "California",
            "CO": "Colorado",
            "CT": "Connecticut",
            "DE": "Delaware",
            "DC": "District Of Columbia",
            "FM": "Federated States Of Micronesia",
            "FL": "Florida",
            "GA": "Georgia",
            "GU": "Guam",
            "HI": "Hawaii",
            "ID": "Idaho",
            "IL": "Illinois",
            "IN": "Indiana",
            "IA": "Iowa",
            "KS": "Kansas",
            "KY": "Kentucky",
            "LA": "Louisiana",
            "ME": "Maine",
            "MH": "Marshall Islands",
            "MD": "Maryland",
            "MA": "Massachusetts",
            "MI": "Michigan",
            "MN": "Minnesota",
            "MS": "Mississippi",
            "MO": "Missouri",
            "MT": "Montana",
            "NE": "Nebraska",
            "NV": "Nevada",
            "NH": "New Hampshire",
            "NJ": "New Jersey",
            "NM": "New Mexico",
            "NY": "New York",
            "NC": "North Carolina",
            "ND": "North Dakota",
            "MP": "Northern Mariana Islands",
            "OH": "Ohio",
            "OK": "Oklahoma",
            "OR": "Oregon",
            "PW": "Palau",
            "PA": "Pennsylvania",
            "PR": "Puerto Rico",
            "RI": "Rhode Island",
            "SC": "South Carolina",
            "SD": "South Dakota",
            "TN": "Tennessee",
            "TX": "Texas",
            "UT": "Utah",
            "VT": "Vermont",
            "VI": "Virgin Islands",
            "VA": "Virginia",
            "WA": "Washington",
            "WV": "West Virginia",
            "WI": "Wisconsin",
            "WY": "Wyoming"
        };
       
        
        var addressObjectArray = [];
        var jurisdictionsArray = [];
        var billingAccount = {};
        
        
        // mapping correlationId
        finalObject.correlationId = correlationId;
        
        //mapping addresses
        if (typeof addresses != 'undefined' && Array.isArray(addresses)) {
            addresses.forEach(function (arrayItem) {
                var addressObject = {};
                //map address id
                if (arrayItem.number !== "NULL") {
                    addressObject.id = arrayItem.number.toString();
                } else {
                    addressObject.id = "";
                }
                
                //map address name
                if (arrayItem.name !== "NULL") {
                    addressObject.name = arrayItem.name.toString();
                } else {
                    addressObject.name = "";
                }
                
                 //map address line1 
                if (arrayItem.line1 !== "NULL") {
                    addressObject.line1 = arrayItem.line1;
                } else {
                    addressObject.line1 = "";
                }
                
                //map address line2 
                if (arrayItem.line2 !== "NULL") {
                    addressObject.line2 = arrayItem.line2.toString();
                } else {
                    addressObject.line2 = "";
                }
            
                //map address line3 
                addressObject.line3 = "";
                //map address line4 
                addressObject.line4 = "";
                
                //map address city 
                if (arrayItem.line3 !== "NULL" && arrayItem.line3.slice(0, -3) !== null) {
                    addressObject.city = arrayItem.line3.slice(0, -3);
                } else {
                    addressObject.city = "";
                }
                
                //map address state
                if (arrayItem.line3 !== "NULL" && arrayItem.line3.slice(-2) !== null) {
                    var state = arrayItem.line3.slice(-2);
                    addressObject.state = stateCodes[state];
                } else {
                    addressObject.state = "";
                }
                
                //map address country
                addressObject.country = "United States";
                
                //map postal code
                if (arrayItem.zip != "NULL" && arrayItem.zip4 !== "NULL") {
                    addressObject.postalCode = arrayItem.zip.toString()+ '-' + arrayItem.zip4;
                } else if (arrayItem.zip !== "NULL"){
                    addressObject.postalCode = arrayItem.zip.toString();
                } else {
                    addressObject.postalCode = "";
                }
                
                //map address type
                if (arrayItem.type !== "NULL") {
                    var type = arrayItem.type;
                    addressObject.type = addressTypes[type];
                } else {
                    addressObject.type = "";
                }
                
                //map jurisdictions
                if (arrayItem.type == "J") {
                    var jurisdictions = accountElement.taxJurisdictions.TaxJurisdictionElement;
                    if (Array.isArray(jurisdictions)) {
                        
                        jurisdictions.forEach(function (items) {
                            var jurisdictionsObject = {}; 
                            // mapping type
                            jurisdictionsObject.type = items.type;
                            
                            //mapping code
                            jurisdictionsObject.code = items.code;
                            
                            jurisdictionsArray.push(jurisdictionsObject);
                        });
                    } else {
                        var jurisdictionsObject = {}; 
                        // mapping type
                        jurisdictionsObject.type = jurisdictions.type;
                        
                        //mapping code
                        jurisdictionsObject.code = jurisdictions.code;
                    
                        jurisdictionsArray.push(jurisdictionsObject);
                    }
                    addressObject.jurisdictions = jurisdictionsArray;
                }
                addressObjectArray.push(addressObject);
            });
            billingAccount.addresses = addressObjectArray;
        } else if (typeof addresses != 'undefined') {
            var addressObject = {};
            //map address id
            if (addresses.number !== "NULL") {
                addressObject.id = addresses.number.toString();
            } else {
                addressObject.id = "";
            }
            
            //map address name
            if (addresses.name !== "NULL") {
                addressObject.name = addresses.name.toString();
            } else {
                addressObject.name = "";
            }
            
             //map address line1 
            if (addresses.line1 !== "NULL") {
                addressObject.line1 = addresses.line1;
            } else {
                addressObject.line1 = "";
            }
            
            //map address line2 
            if (addresses.line2 !== "NULL") {
                addressObject.line2 = addresses.line2.toString();
            } else {
                addressObject.line2 = "";
            }
        
            //map address line3 
            addressObject.line3 = "";
            //map address line4 
            addressObject.line4 = "";
            
            //map address city 
            if (addresses.line3 !== "NULL" && addresses.line3.slice(0, -3) !== null) {
                addressObject.city = addresses.line3.slice(0, -3);
            } else {
                addressObject.city = "";
            }
            
            //map address state
            if (addresses.line3 !== "NULL" && addresses.line3.slice(-2) !== null) {
                var state = addresses.line3.slice(-2);
                addressObject.state = stateCodes[state];
            } else {
                addressObject.state = "";
            }
            
            //map address country
            addressObject.country = "United States";
            
            //map postal code
            if (addresses.zip != "NULL" && addresses.zip4 !== "NULL") {
                addressObject.postalCode = addresses.zip.toString()+ '-' + addresses.zip4;
            } else if (addresses.zip !== "NULL"){
                addressObject.postalCode = addresses.zip.toString();
            } else {
                addressObject.postalCode = "";
            }
            
            //map address type
            if (addresses.type !== "NULL") {
                var type = addresses.type;
                addressObject.type = addressTypes[type];
            } else {
                addressObject.type = "";
            }
            
            //map jurisdictions
            if (addresses.type == "J") {
                var jurisdictions = accountElement.taxJurisdictions.TaxJurisdictionElement;
                if (Array.isArray(jurisdictions)) {
                    jurisdictions.forEach(function (items) {
                        var jurisdictionsObject = {}; 
                        // mapping type
                        jurisdictionsObject.type = items.type;
                        
                        //mapping code
                        jurisdictionsObject.code = items.code;
                        
                        jurisdictionsArray.push(jurisdictionsObject);
                    });
                } else {
                    var jurisdictionsObject = {}; 
                    // mapping type
                    jurisdictionsObject.type = jurisdictions.type;
                    
                    //mapping code
                    jurisdictionsObject.code = jurisdictions.code;
                
                    jurisdictionsArray.push(jurisdictionsObject);
                }
                addressObject.jurisdictions = jurisdictionsArray;
            }
            addressObjectArray.push(addressObject);
            billingAccount.addresses = addressObjectArray;
        }
        
        billingAccount.billingId = billingId.toString();
        
        // phones mapping
         var phoneTypes = {
            4: "MOBILE",
            1: "HOME",
            2: "WORK"
        };
        var phonesObjectArray = [];
        var requestPhones = request.phones;
        var phoneTypes = [];
        var cellPhone = responseBody.accounts.AccountElement.cellPhone;
        var homePhone = responseBody.accounts.AccountElement.homePhone;
        var workPhone = responseBody.accounts.AccountElement.workPhone;
        if (typeof requestPhones != 'undefined') {
            requestPhones.forEach(function(phone){
                var phoneObject = {};
                if (phone.type == "HOME") {
                    phoneObject.type = phone.type;
                    phoneObject.countryCode = "1";
                    if (homePhone.areaCode != "NULL") {
                        phoneObject.number = homePhone.areaCode + '' + homePhone.prefix + '' + homePhone.suffix;
                    }
                } else if (phone.type == "WORK") {
                    phoneObject.type = phone.type;
                    phoneObject.countryCode = "1";
                    if (homePhone.areaCode != "NULL") {
                        phoneObject.number = workPhone.areaCode + '' + workPhone.prefix + '' + workPhone.suffix;
                    }
                } else if (phone.type == "MOBILE") {
                    phoneObject.type = phone.type;
                    phoneObject.countryCode = "1";
                    if (homePhone.areaCode != "NULL") {
                        phoneObject.number = cellPhone.areaCode + '' + cellPhone.prefix + '' + cellPhone.suffix;
                    }
                }
                phonesObjectArray.push(phoneObject);
            });
            billingAccount.phones = phonesObjectArray;
        }
        // mapping billingAccount object to final response
        finalObject.billingAccount = billingAccount;
    }
    context.setVariable("finalObject", JSON.stringify(finalObject));
}


