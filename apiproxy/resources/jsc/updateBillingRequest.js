var getPayload = context.getVariable("request.content");
var payload = JSON.parse(getPayload);
// States
var stateCodes = {
    "Alabama": "AL",
    "Alaska": "AK",
    "American Samoa": "AS",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "District Of Columbia": "DC",
    "Federated States Of Micronesia": "FM",
    "Florida": "FL",
    "Georgia": "GA",
    "Guam": "GU",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Marshall Islands": "MH",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Northern Mariana Islands": "MP",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Palau": "PW",
    "Pennsylvania": "PA",
    "Puerto Rico": "PR",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virgin Islands": "VI",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY"
};
if (typeof payload != 'undefined') {
    var phones = payload.billingAccount.phones;
    var addresses = payload.billingAccount.addresses;
    var billingAccount = payload.billingAccount;
    context.setVariable("initialRequest", JSON.stringify(billingAccount));
}
var hasDuplicatePhones = false;
var hasInvalidAddresses = false;
if (typeof phones !== 'undefined') {
    var phonesLength = phones.length; 
    if (phonesLength > 1) {
        phones.map(v => v.type).sort().sort((a, b) => {
            if (a === b) {
                hasDuplicatePhones = true;
                context.setVariable("invalidPhones", "Please provide unique phone types");
            }
        })
    }
} 

if (typeof addresses !== 'undefined') {
    var addressesLength = addresses.length; 
    if (addressesLength > 1) {
        addresses.map(v => v.type).sort().sort((a, b) => {
            if (a === b) {
                hasInvalidAddresses = true;
                context.setVariable("invalidAddress", "Please provide different addresses (if one is TAX then other should be MAILING)");
            } 
        })
        
    } 
    if (hasInvalidAddresses === false) {
        addresses.forEach(function (arrayItem) {
            if (arrayItem.type == "TAX" && typeof arrayItem.jurisdictions === 'undefined') {
                hasInvalidAddresses = true;
                context.setVariable("invalidAddress", "Please provide 'jurisdictions' in TAX address");
                
            } 
            if (arrayItem.type == "TAX") {
                var taxState = stateCodes[arrayItem.state];
                context.setVariable("taxState", taxState);
            } else {
                var mailingState = stateCodes[arrayItem.state];
                context.setVariable("mailingState", mailingState);
            }
        });
        
    }
} 
    
context.setVariable("hasDuplicatePhones", hasDuplicatePhones);
context.setVariable("hasInvalidAddresses", hasInvalidAddresses);
context.setVariable("request.verb", "POST");
