var billingId = context.getVariable("apigee.billingId");

if (billingId !== null ){
    var regex = new RegExp(/^[0-9]\d{3,}$/);
    if (regex.test(billingId)) {
        context.setVariable("isValidBillingId", true);
    } else {
        context.setVariable("isValidBillingId", false);
        context.setVariable("billingIDMessage", "Please provide a valid billingId (numeric value and minimum length should be 4)");
    }
} else {
    context.setVariable("isValidBillingId", true);
    context.setVariable("billingIDMessage", "Please provide a billingId");
}
var timeStamp = new Date().toISOString();
context.setVariable("timeStamp", timeStamp);