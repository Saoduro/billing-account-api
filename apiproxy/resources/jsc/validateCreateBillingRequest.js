var getPayload = context.getVariable("request.content");
var payload = JSON.parse(getPayload);
var hasDuplicatePhones = false;
if (typeof payload != 'undefined') {
    var billingAccounts = payload.billingAccounts;
    billingAccounts.forEach(function(billingAccount)  {
        var phones = billingAccount.phones;
        if (typeof phones !== 'undefined') {
            var phonesLength = phones.length; 
            if (phonesLength > 1) {
                phones.map(v => v.type).sort().sort((a, b) => {
                    if (a === b) {
                        hasDuplicatePhones = true;
                        context.setVariable("invalidPhones", "Please provide unique phone types");
                    }
                });
                
            }
            if (hasDuplicatePhones) {
                    return;
                }
        }
    });
}

context.setVariable("hasDuplicatePhones", hasDuplicatePhones);