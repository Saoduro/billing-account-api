<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xslt="http://xml.apache.org/xslt">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" xslt:indent-amount="2"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="usernameTokenId" select="''"/>
	<xsl:param name="username" select="''"/>
	<xsl:param name="password" select="''"/>
	<xsl:param name="requestId" select="''"/>
	<xsl:param name="effectiveDate" select="''"/>
	<!--
      <xsl:template match="/">
      <xsl:apply-templates/>
      </xsl:template>
  -->
	<xsl:template match="/soap:Envelope">
		<soap:Envelope>
			<soap:Header>
				<xsl:choose>
					<xsl:when test="soap:Header">
						<xsl:apply-templates select="soap:Header"/>
					</xsl:when>
					<xsl:otherwise>
						<wsse:Security>
							<xsl:call-template name="injectUsernameToken"/>
						</wsse:Security>
					</xsl:otherwise>
				</xsl:choose>
			</soap:Header>
			<soap:Body>
				<xsl:apply-templates select="soap:Body"/>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>
	<xsl:template match="soap:Body">
		<NS1:addCustomer xmlns:NS1="http://services.oam.xcel.com">
			<addAccountRequestElement>
				<endUser>
					<endUserID>apigee</endUserID>
					<endUserType>A</endUserType>
				</endUser>
				<requestID>
					<xsl:value-of select="$requestId"/>
				</requestID>
				<type>
					<xsl:value-of select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name()='type']"/>
				</type>
				<companyName>
					<xsl:value-of select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name()='companyName']"/>
				</companyName>
				<email>
					<xsl:value-of select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name()='email']"/>
				</email>
				<addresses>
					<xsl:for-each select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name() ='addresses']">
						<xsl:variable name="addressType" select="./*[local-name() = 'type']"/>
						<xsl:if test="$addressType = 'MAILING'">
							<AddressElement>
								<!--<xsl:variable name="addressTypeCode">
								<xsl:choose>
									<xsl:when test="$addressType = 'MAILING'">
										<xsl:value-of select="'A'"/>
									</xsl:when>
									<xsl:when test="$addressType = 'SERVICE'">
										<xsl:value-of select="'J'"/>
									</xsl:when>
									<xsl:when test="$phoneType = 'TAX'">
										<xsl:value-of select="'X'"/>
									</xsl:when>
								</xsl:choose>
							</xsl:variable>-->
								<type>
									<xsl:value-of select="'A'"/>
									<!--<xsl:value-of select="$addressTypeCode"/>-->
								</type>
								<coName/>
								<deliveryBarCode/>
								<effectiveDate>
									<xsl:value-of select="$effectiveDate"/>
								</effectiveDate>
								<expirationDate/>
								<line1>
									<xsl:value-of select="./*[local-name() ='line1']"/>
								</line1>
								<line2/>
								<line3>
									<xsl:value-of select="concat(./*[local-name() = 'city'], ' ', ./*[local-name() = 'state'])"/>
								</line3>
								<name>
									<xsl:value-of select="concat(/soap:Envelope/soap:Body/child::*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name() = 'firstName'], ' ', /soap:Envelope/soap:Body/child::*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name() = 'lastName'])"/>
								</name>
								<number>0</number>
								<typeDescription>Billing and Delivery Address</typeDescription>
								<zip>
									<xsl:value-of select="substring(./*[local-name() = 'postalCode'],1, 5)"/>
								</zip>
								<zip4>
									<xsl:value-of select="substring(./*[local-name() = 'postalCode'],7, 4)"/>
								</zip4>
								<modifiable>1</modifiable>
							</AddressElement>
						</xsl:if>
					</xsl:for-each>
				</addresses>
				<phones>
					<xsl:for-each select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name()='phones']">
						<PhoneElement>
							<areaCode>
								<xsl:value-of select="substring(./*[local-name() = 'number'],1,3)"/>
							</areaCode>
							<prefix>
								<xsl:value-of select="substring(./*[local-name() = 'number'],4,3)"/>
							</prefix>
							<xsl:variable name="phoneType" select="./*[local-name() = 'type']"/>
							<purposeDescription>
								<xsl:value-of select="$phoneType"/>
							</purposeDescription>
							<purposeCode>
								<xsl:choose>
									<xsl:when test="$phoneType = 'HOME'">
										<xsl:value-of select="'1'"/>
									</xsl:when>
									<xsl:when test="$phoneType = 'MOBILE'">
										<xsl:value-of select="'4'"/>
									</xsl:when>
									<xsl:when test="$phoneType = 'WORK'">
										<xsl:value-of select="'2'"/>
									</xsl:when>
								</xsl:choose>
							</purposeCode>
							<suffix>
								<xsl:value-of select="substring(./*[local-name() = 'number'],7,4)"/>
							</suffix>
						</PhoneElement>
					</xsl:for-each>
				</phones>
				<firstName>
					<xsl:value-of select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name() = 'firstName']"/>
				</firstName>
				<lastName>
					<xsl:value-of select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name() = 'lastName']"/>
				</lastName>
				<debtorBranchId>
					<xsl:value-of select="./*[local-name() = 'addCustomer']/*[local-name()='billingAccounts']/*[local-name() = 'debtorBranch']"/>
				</debtorBranchId>
			</addAccountRequestElement>
		</NS1:addCustomer>
	</xsl:template>
	<xsl:template match="soap:Header">
		<xsl:choose>
			<xsl:when test='wsse:Security'>
				<xsl:apply-templates select="*"/>
			</xsl:when>
			<xsl:otherwise>
				<wsse:Security>
					<xsl:call-template name="injectUsernameToken"/>
				</wsse:Security>
				<xsl:copy-of select="*"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<!--<xsl:template match="billingAccounts"/>-->
	<!--<xsl:template match="soap:Header/wsse:Security">
    <wsse:Security>
      <xsl:call-template name="injectUsernameToken"/>
      <xsl:copy-of select="*" />
    </wsse:Security>
  </xsl:template>-->
	<xsl:template match="soap:Header/wsse:Security/wsse:UsernameToken"/>
	<xsl:template name='injectUsernameToken'>
		<wsse:UsernameToken>
			<xsl:attribute name="Id">
				<xsl:value-of select="$usernameTokenId"/>
			</xsl:attribute>
			<wsse:Username>
				<xsl:value-of select="$username"/>
			</wsse:Username>
			<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">
				<xsl:value-of select="$password"/>
			</wsse:Password>
		</wsse:UsernameToken>
	</xsl:template>
</xsl:stylesheet>