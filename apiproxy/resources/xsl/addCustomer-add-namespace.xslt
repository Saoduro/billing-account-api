<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" version="1.0">

    <xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>

    <!-- Stylesheet to inject namespaces into a document in specific places -->
    <xsl:template match="/">
        <soap:Envelope>
            <soap:Header>
                      <!--<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
         <wsse:UsernameToken wsu:Id="UsernameToken-13178038" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
            <wsse:Username>oamwscldev1</wsse:Username>
            <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">Spr!ng0nine</wsse:Password>
         </wsse:UsernameToken>
      </wsse:Security>-->
            </soap:Header>
            <soap:Body>
                <xsl:choose>
                    <!-- Handle 'Root' wrapper added by JSON to XML policy -->
                    <!--<xsl:when test="normalize-space(/Root)">
                        <NS1:addCustomer xmlns:NS1="http://services.oam.xcel.com">
                            <xsl:apply-templates select="node()|@*"/>
                        </NS1:addCustomer>
                    </xsl:when>-->
                    <!-- Handle 'Array' wrapper added by JSON to XML policy -->
                    <!--<xsl:when test="normalize-space(/Array)">
                        <NS1:addCustomer xmlns:NS1="http://services.oam.xcel.com">
                            <xsl:apply-templates select="node()|@*"/>
                        </NS1:addCustomer>
                    </xsl:when>-->
                    <!-- If the root element is not what was in the schema, add it -->
                    <xsl:when test="not(normalize-space(/addCustomer))">
                        <NS1:addCustomer xmlns:NS1="http://services.oam.xcel.com">
                            <xsl:apply-templates select="node()|@*"/>
                        </NS1:addCustomer>
                    </xsl:when>
                    <!-- everything checks out,  just copy the xml-->
                    <xsl:otherwise>
                        <xsl:apply-templates select="node()|@*"/>
                    </xsl:otherwise>
                </xsl:choose>
            </soap:Body>
        </soap:Envelope>
    </xsl:template>

    <xsl:template match="/Root/*" name="copy-root">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="namespace::*"/>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
    </xsl:template>
    
    <!--<xsl:template match="/Array/*" name="copy-array">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="namespace::*"/>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
    </xsl:template>-->
    
    <xsl:template match="*[not(local-name()='Root') and not(local-name()='Array')]" name="copy-all">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- template to copy the rest of the nodes -->
    <xsl:template match="comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>
</xsl:stylesheet>
