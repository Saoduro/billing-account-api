<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://services.oam.xcel.com" xmlns:xslt="http://xml.apache.org/xslt">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" xslt:indent-amount="2"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="usernameTokenId" select="''"/>
	<xsl:param name="username" select="''"/>
	<xsl:param name="password" select="''"/>
	<xsl:param name="effectiveDate" select="''"/>
	<xsl:param name="billingId" select="''"/>
	<xsl:param name="mailingState" select="''"/>
	<xsl:param name="taxState" select="''"/>
	<xsl:param name="correlationId" select="''"/>
	
	<xsl:template match="/soap:Envelope">
		<soap:Envelope>
			<soap:Header>
				<xsl:choose>
					<xsl:when test="soap:Header">
						<xsl:apply-templates select="soap:Header"/>
					</xsl:when>
					<xsl:otherwise>
						<wsse:Security>
							<xsl:call-template name="injectUsernameToken"/>
						</wsse:Security>
					</xsl:otherwise>
				</xsl:choose>
			</soap:Header>
			<soap:Body>
				<xsl:apply-templates select="soap:Body"/>
			</soap:Body>
		</soap:Envelope>
	</xsl:template>
	<xsl:template match="soap:Body">
		<ser:updateAccount>
			<accountRequestElement>
				<endUser>
					<endUserID>apigee</endUserID>
					<endUserType>A</endUserType>
				</endUser>
				<requestID>
					<xsl:value-of select="$correlationId"/>
				</requestID>
				<number>
					<xsl:value-of select="$billingId"/>
				</number>
				
				<!--Addresses Mapping-->
				
				<xsl:variable name="addresses" select="./*[local-name() = 'updateAccount']/*[local-name()='billingAccount']/*[local-name() ='addresses']"/>
				<xsl:if test="normalize-space($addresses) != ''">
                    <addresses>
					<xsl:for-each select="./*[local-name() = 'updateAccount']/*[local-name()='billingAccount']/*[local-name() ='addresses']">
					<xsl:variable name="addressType" select="./*[local-name() = 'type']"/>
						<AddressElement>
						    <type>
    							<xsl:choose>
    								<xsl:when test="$addressType = 'MAILING'">
    									<xsl:value-of select="'A'"/>
    								</xsl:when>
    								<xsl:when test="$addressType = 'TAX'">
    									<xsl:value-of select="'J'"/>
    								</xsl:when>
    							</xsl:choose>
    						</type>
							<effectiveDate>
								<xsl:value-of select="$effectiveDate"/>
							</effectiveDate>
							<line1>
								<xsl:value-of select="./*[local-name() ='line1']"/>
							</line1>
							
							<line2>
							    <xsl:variable name="addressLine" select="./*[local-name() = 'line2']"/>
							    <xsl:if test="normalize-space($addressLine) != ''">
                                    <xsl:value-of select="./*[local-name() ='line2']"/>  
                                </xsl:if>
							</line2>
							<line3>
							    <xsl:if test="$addressType = 'MAILING'">
                                    <xsl:value-of select="concat(./*[local-name() = 'city'], ' ', $mailingState)"/>
                                </xsl:if>
                                <xsl:if test="$addressType = 'TAX'">
                                    <xsl:value-of select="concat(./*[local-name() = 'city'], ' ', $taxState)"/>
                                </xsl:if>
							</line3>
							<name>
								<xsl:value-of select="./*[local-name() ='name']"/>
							</name>
							<number>0</number>
							<zip>
								<xsl:value-of select="substring(./*[local-name() = 'postalCode'],1, 5)"/>
							</zip>
							<zip4>
								<xsl:value-of select="substring(./*[local-name() = 'postalCode'],7, 4)"/>
							</zip4>
						</AddressElement>
					</xsl:for-each>
					</addresses>
					<!--Tax Jurisdictions Mapping-->
					<xsl:for-each select="./*[local-name() = 'updateAccount']/*[local-name()='billingAccount']/*[local-name() ='addresses']">
					    <xsl:variable name="addressType" select="./*[local-name() = 'type']"/>
					    <xsl:if test="$addressType = 'TAX'">
    					<taxJurisdictions>
                            <xsl:for-each select="./*[local-name() = 'jurisdictions']">
                                <TaxJurisdictionElement>
                                    <type>
        								<xsl:value-of select="./*[local-name() = 'type']"/>
        							</type>
        							<code>
        								<xsl:value-of select="./*[local-name() = 'code']"/>
        							</code>
        							<effectiveDate>
    									<xsl:value-of select="$effectiveDate"/>
    								</effectiveDate>
                                </TaxJurisdictionElement>
                            </xsl:for-each>
                        </taxJurisdictions>
                    </xsl:if>    
					</xsl:for-each>
                </xsl:if>
                
                <!--Phones Mapping-->
				
				<xsl:variable name="phones" select="./*[local-name() = 'updateAccount']/*[local-name()='billingAccount']/*[local-name() ='phones']"/>
				<xsl:if test="normalize-space($phones) != ''">
                    <phones>
					<xsl:for-each select="./*[local-name() = 'updateAccount']/*[local-name()='billingAccount']/*[local-name()='phones']">
						<PhoneElement>
							<areaCode>
								<xsl:value-of select="substring(./*[local-name() = 'number'],1,3)"/>
							</areaCode>
							<prefix>
								<xsl:value-of select="substring(./*[local-name() = 'number'],4,3)"/>
							</prefix>
							<xsl:variable name="phoneType" select="./*[local-name() = 'type']"/>
							<!--<type>
								<xsl:choose>
									<xsl:when test="$phoneType = 'HOME'">
										<xsl:value-of select="'1'"/>
									</xsl:when>
									<xsl:when test="$phoneType = 'MOBILE'">
										<xsl:value-of select="'4'"/>
									</xsl:when>
									<xsl:when test="$phoneType = 'WORK'">
										<xsl:value-of select="'2'"/>
									</xsl:when>
								</xsl:choose>
							</type>-->
							<purposeCode>
								<xsl:choose>
									<xsl:when test="$phoneType = 'HOME'">
										<xsl:value-of select="'1'"/>
									</xsl:when>
									<xsl:when test="$phoneType = 'MOBILE'">
										<xsl:value-of select="'4'"/>
									</xsl:when>
									<xsl:when test="$phoneType = 'WORK'">
										<xsl:value-of select="'2'"/>
									</xsl:when>
								</xsl:choose>
							</purposeCode>
							<suffix>
								<xsl:value-of select="substring(./*[local-name() = 'number'],7,4)"/>
							</suffix>
						</PhoneElement>
					</xsl:for-each>
				</phones>
                </xsl:if>
			</accountRequestElement>
		</ser:updateAccount>
	</xsl:template>
	<xsl:template match="soap:Header">
		<xsl:choose>
			<xsl:when test='wsse:Security'>
				<xsl:apply-templates select="*"/>
			</xsl:when>
			<xsl:otherwise>
				<wsse:Security>
					<xsl:call-template name="injectUsernameToken"/>
				</wsse:Security>
				<xsl:copy-of select="*"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="soap:Header/wsse:Security/wsse:UsernameToken"/>
	<xsl:template name='injectUsernameToken'>
		<wsse:UsernameToken>
			<xsl:attribute name="Id">
				<xsl:value-of select="$usernameTokenId"/>
			</xsl:attribute>
			<wsse:Username>
				<xsl:value-of select="$username"/>
			</wsse:Username>
			<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">
				<xsl:value-of select="$password"/>
			</wsse:Password>
		</wsse:UsernameToken>
	</xsl:template>
</xsl:stylesheet>