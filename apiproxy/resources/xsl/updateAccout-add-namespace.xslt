<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ser="http://services.oam.xcel.com" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" version="1.0">

    <xsl:output encoding="utf-8" indent="yes" method="xml" omit-xml-declaration="yes"/>

    <!-- Stylesheet to inject namespaces into a document in specific places -->
    <xsl:template match="/">
        <soap:Envelope>
            <soap:Header>
            </soap:Header>
            <soap:Body>
                <xsl:choose>
                    <xsl:when test="not(normalize-space(/updateAccount))">
                        <ser:updateAccount>
                            <xsl:apply-templates select="node()|@*"/>
                        </ser:updateAccount>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="node()|@*"/>
                    </xsl:otherwise>
                </xsl:choose>
            </soap:Body>
        </soap:Envelope>
    </xsl:template>

    <xsl:template match="/Root/*" name="copy-root">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="namespace::*"/>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="*[not(local-name()='Root') and not(local-name()='Array')]" name="copy-all">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="node()"/>
        </xsl:element>
    </xsl:template>

    <!-- template to copy the rest of the nodes -->
    <xsl:template match="comment() | processing-instruction()">
        <xsl:copy/>
    </xsl:template>
</xsl:stylesheet>
